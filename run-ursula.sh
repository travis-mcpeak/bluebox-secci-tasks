#!/bin/sh

# this script is run to get container at bare minimum needed to copy deployer script
sudo apt-get update
sudo apt-get -y install openssh-client libssl-dev
echo "$DEPLOYER_KEY" > deployer_key.pem
mkdir ~/.ssh
echo "$DEPLOYER_KNOWN_HOST" > ~/.ssh/known_hosts
chmod 0400 deployer_key.pem

echo "export OS_PASSWORD=$OS_PASSWORD" >> .stackrc
echo "export OS_TENANT_ID=$OS_TENANT_ID" >> .stackrc
echo "export OS_TENANT_NAME=$OS_TENANT_NAME" >> .stackrc
echo "export OS_PROJECT_NAME=$OS_PROJECT_NAME" >> .stackrc
echo "export OS_USERNAME=$OS_USERNAME" >> .stackrc
echo "export OS_CACERT=$OS_CACERT" >> .stackrc
echo "export OS_AUTH_URL=$OS_AUTH_URL" >> .stackrc
echo "export IMAGE_ID=$IMAGE_ID" >> .stackrc

export AUTOMATION_DIR="/home/$DEPLOYER_USER/security-automation"

# need sudo because Lynis is going to have some root owned things in here later
ssh -i deployer_key.pem $DEPLOYER_USER@$DEPLOYER_HOST "sudo rm -rf $AUTOMATION_DIR"
ssh -i deployer_key.pem $DEPLOYER_USER@$DEPLOYER_HOST "mkdir $AUTOMATION_DIR"

scp -i deployer_key.pem sec-ci-tasks/deployer-script.sh $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR
scp -i deployer_key.pem sec-ci-tasks/undercloud-bluebox.open-test.ibmcloud.com.crt $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR
scp -i deployer_key.pem .stackrc $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR

ssh -i deployer_key.pem -n -o TCPKeepAlive=yes -o ServerAliveInterval=50 $DEPLOYER_USER@$DEPLOYER_HOST "cd $AUTOMATION_DIR; bash deployer-script.sh"

scp -i deployer_key.pem -r $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR/env-details .
