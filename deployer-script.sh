#!/bin/bash

mkdir env-details

sudo apt-get install -y git python-virtualenv openssl libssl-dev

git clone https://github.com/blueboxgroup/ursula

# this installs things, needs to be sudo
sudo sh ursula/bootstrap.sh

source ~/ursula-venv/bin/activate

pip install -U pip
pip install -r ursula/requirements.txt
pip install --upgrade setuptools

source .stackrc
# need absolute path for cert
export OS_CACERT=`readlink -f $OS_CACERT`

# this file is expected to exist
touch ~/.stackrc

# deploy OpenStack to test environment
export USE_PRIVATE_IPS="true"
cd ursula
test/setup
test/run

# get environment details out for Lynis to use
cd ..
cp ursula/envs/test/int-test.pem env-details/
cat ursula/envs/test/hosts | grep -A 3 "compute" | tail -n +2 >> env-details/hosts
cd env-details
tar -cvzf env-details.tgz int-test.pem hosts
