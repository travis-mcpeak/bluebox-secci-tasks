#!/bin/sh

sudo apt-get update
sudo apt-get -y install openssh-client

export AUTOMATION_DIR="/home/$DEPLOYER_USER/security-automation"

echo "$DEPLOYER_KEY" > deployer_key.pem
mkdir ~/.ssh
echo "$DEPLOYER_KNOWN_HOST" > ~/.ssh/known_hosts
chmod 0400 deployer_key.pem

echo "config:license_key:$LYNIS_LIC_KEY:" >> custom.prf
echo "config:upload_server:$LYNIS_REPORT_SRV:" >> custom.prf

ssh -i deployer_key.pem $DEPLOYER_USER@$DEPLOYER_HOST "mkdir -p $AUTOMATION_DIR"

scp -i deployer_key.pem custom.prf $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR
scp -i deployer_key.pem env-details/env-details.tgz $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR
scp -i deployer_key.pem sec-ci-tasks/deployer-lynis.sh $DEPLOYER_USER@$DEPLOYER_HOST:$AUTOMATION_DIR

ssh -i deployer_key.pem -n -o TCPKeepAlive=yes -o ServerAliveInterval=50 $DEPLOYER_USER@$DEPLOYER_HOST "cd $AUTOMATION_DIR; bash deployer-lynis.sh"
