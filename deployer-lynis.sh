#!/bin/bash

sudo apt-get update
sudo apt-get -y install wget openssh-client

echo "Getting Lynis"
wget https://cisofy.com/files/lynis-2.2.0.tar.gz
tar -xvzf lynis-2.2.0.tar.gz
cat custom.prf >> lynis/default.prf

tar -xvf env-details.tgz
chmod 0400 int-test.pem

while read host; do
  echo "--- Copying Lynis to $host ---"
  scp -q -i int-test.pem -r -o StrictHostKeyChecking=no lynis/ ubuntu@$host:/home/ubuntu
  echo "--- Running Lynis on $host ---"
  # Lynis has protections that prevent running scripts as root that root doesn't own, so we change owner to root
  # Lynis is removed after running
  ssh -n -i int-test.pem -o StrictHostKeyChecking=no ubuntu@$host 'sudo chown -R 0:0 lynis; cd lynis; sudo ./lynis audit system --quick --upload; cd ..; sudo rm -rf lynis'
done <hosts
